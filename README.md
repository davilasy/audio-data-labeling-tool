This repository contains the code used to create the audio data annotation tool to label speech intonations. Here, you will also find the executables to run the tool as a stand-alone application. The code and executable were developed in MATLAB 2019b using their Design application.

This infrastructure can be use to label any other type of 1-D signal, but could be modified to support the display of 2-D and 3-D signals (i.e., images and videos, respectively).

Content of this repository:
AudioLab.prj
- Project containing the code for the design of the GUI
- To modify the GUI, open this project in your prefered MATLAB version

import_file_info.m
- This is a support function to that imports a file with information about the data that will be displayed and labeled
- This function will need to be modified to accomodate the needs of the data that wants to be labeled

import_file_annotation.m
- This is a function that reads the file that will contain or that contains the labels to the data of interest

Folder "AudioLab"
- Contains the executables to run this tool as a stand-alone application
- To run this app, MATLAB 2019b is needed or its run-time application. See details on folder.


